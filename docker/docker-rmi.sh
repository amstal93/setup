#!/bin/bash

docker rmi $(docker images | grep "${@}" | awk '{print $1 ":" $2}')
