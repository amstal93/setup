######################################################################################################################################################

# @project        Lucas ▸ Setup
# @file           .zshrc
# @author         Lucas Brémond <lucas.bremond@gmail.com>
# @license        N/A

######################################################################################################################################################

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
export EDITOR='nano'

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

setopt HIST_IGNORE_SPACE

HISTFILE=~/.zsh_history
HISTSIZE=999999999
SAVEHIST=${HISTSIZE}

export CORRECT_IGNORE_FILE='.*'

######################################################################################################################################################

# Shell

# https://stackoverflow.com/questions/60729605/word-forward-backward-delimiter-difference-between-bash-and-zsh
export WORDCHARS="${WORDCHARS/\//}"

## Starship

eval "$(starship init zsh)"

source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

# Load Git completion
zstyle ':completion:*:*:git:*' script ~/.zsh/git-completion.bash
fpath=(~/.zsh $fpath)

autoload -Uz compinit && compinit

# Color completion for some things.
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# Select with arrow keys.
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' list-colors ''
zstyle ':completion:*:*:*:*:*' menu select

# Faster completion.
zstyle ':completion::complete:*' use-cache 1

# Disable git info for mounted directories
# https://github.com/sindresorhus/pure/issues/189#issuecomment-173844254
zstyle ':vcs_info:*' disable-patterns "/mnt(|/*)"

######################################################################################################################################################

# Utilities

## thefuck

eval $(thefuck --alias)

## ranger

alias rg='. ranger'

kc () {
    kubectl config get-contexts | tail -n +2 | fzf | cut -d" " -f2- | awk '{print $1}' | xargs kubectl config use-context
}

## exa

if [ "$(command -v exa)" ]; then
    unalias -m 'll'
    unalias -m 'l'
    unalias -m 'la'
    unalias -m 'ls'
    alias ls='exa -G  --color auto --icons -a -s type'
    alias ll='exa -l --color always --icons -a -s type'
    alias lt='exa --tree --color always --icons -a -s type --level '
fi

## bat

if [ "$(command -v bat)" ]; then
  unalias -m 'cat'
  alias cat='bat -pp --theme="Nord"'
fi

## fzf

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export PATH="/usr/local/opt/helm@2/bin:$PATH"

## git

alias gs="git status"
alias gc="git checkout"
alias gr="git rebase"
alias gcb="git checkout -b"

gcm () {
    git checkout $(git branch | cut -c 3- | grep -E '^master$|^main$') && git pull && git fetch -p
}

## tug

alias tcf="tug commit feat"
alias tcc="tug commit chore"
alias tcb="tug commit build"
alias tct="tug commit test"

## Docker

export DOCKER_BUILDKIT=1
export COMPOSE_DOCKER_CLI_BUILD=1

alias di="docker images"
alias dpsa="docker ps --all --format 'table {{.Names}}\t{{.Status}}\t{{.Image}}\t{{.Ports}}' | less -S"
alias wdpsa="watch docker ps --all"
alias dcp="yes | docker container prune"
alias dvp="yes | docker volume prune"
alias dnp="yes | docker network prune"
alias docker-rm="/Users/lucas/Projects/Personal/Setup/docker/docker-rm.sh"
alias docker-prune="/Users/lucas/Projects/Personal/Setup/docker/docker-prune.sh"
alias docker-rmi="/Users/lucas/Projects/Personal/Setup/docker/docker-rmi.sh"
alias docker-rmi-none="/Users/lucas/Projects/Personal/Setup/docker/docker-rmi-none.sh"
alias docker-rmi-dirty="/Users/lucas/Projects/Personal/Setup/docker/docker-rmi-dirty.sh"
alias dlf="docker logs --follow"
alias lzd="lazydocker"

######################################################################################################################################################

# Shortcuts

alias zshconfig="code ~/.zshrc"
alias ohmyzsh="code ~/.oh-my-zsh"
alias sshconfig="code ~/.ssh/config"

######################################################################################################################################################
